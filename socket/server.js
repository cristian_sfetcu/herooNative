const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 8123;

const places = [
  {latitude: 44.44967, longitude: 26.074335},
  {latitude: 44.450002, longitude: 26.073794},
  {latitude: 44.450998, longitude: 26.072368},
  {latitude: 44.451973, longitude: 26.07214},
];

io.on('connection', (socket) => {
  console.log(socket.id, ' connected !');
  socket.on('create_premium_emergency', (msg) => {
    let index = 0;
    setInterval(() => {
      io.emit('premium_emergency_created', {
        agentName: 'Ionut Franaru',
        location: places[index],
      });
      if (index === 3) {
        index = 0;
      } else {
        index++;
      }
    }, 3000);
  });
});

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});
