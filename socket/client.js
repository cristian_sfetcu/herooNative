var io = require('socket.io-client');
var socket = io.connect('https://heroo-app.herokuapp.com');
//var socket = io.connect('http://localhost:8123');

socket.on('connect', () => {
  console.log('socket.id: ', socket.id);
  setTimeout(() => {
    socket.emit(
      'create_premium_emergency',
      JSON.stringify({id: '61698f991f4255a5dcdbc1ec', location: 'H32G+C8'}),
    );
    console.log(
      JSON.stringify({id: '61698f991f4255a5dcdbc1ec', location: 'H32G+C8'}),
    );
  }, 2000);
});

socket.on('connect_error', (error) => {
  console.log(error.type);
});

socket.on('premium_emergency_created', (message) => {
  console.log('premium_emergency_created with the message ', message);
});

socket.on('disconnect', (reason) => {
  console.log('disconnected', reason);
});
