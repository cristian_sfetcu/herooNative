import React from 'react';

import MapView from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

import { Block } from '../components/';
import { useTheme } from '../hooks/';

const Agreement = () => {
  const { sizes } = useTheme();

  return (
    <View style={styles.container}>
      {/* <Text>Map</Text> */}
      {/* <MapView style={styles.map} region={{
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }} /> */}
      {/* <Text>Map</Text> */}
      <MapView style={StyleSheet.absoluteFillObject} 
      provider={"google"}
      ></MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export default Agreement;
