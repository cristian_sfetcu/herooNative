import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/core';

import { IArticleOptions } from '../constants/types';
import { useData, useTheme, useTranslation } from '../hooks/';
import { Block, Button, Image, Product, Text } from '../components/';

const Rental = () => {
  const { article } = useData();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { gradients, sizes } = useTheme();

  return (
    <Block></Block>
  );
};

export default Rental;
