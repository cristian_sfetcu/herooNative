import React, { useCallback, useEffect, useState} from 'react';
import {Linking, Platform } from 'react-native';
import {useNavigation } from '@react-navigation/core';
import axios from 'axios';
import {useData, useTheme, useTranslation} from '../hooks/';
import * as regex from '../constants/regex';
import {Block, Button, Input, Image, Text, Checkbox} from '../components/';
import {useStoreActions} from 'easy-peasy';
const isAndroid = Platform.OS === 'android';

interface ILogin {
  email: string;
  password: string;
  agreed: boolean;
}
interface ILoginValidation {
  email: boolean;
  password: boolean;
  agreed: boolean;
}

const Login = () => {
  const setUsr = useStoreActions((actions) => actions.setUserName);

  const {t} = useTranslation();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const [isValid, setIsValid] = useState<ILoginValidation>({
    email: false,
    password: false,
    agreed: false,
  });
  const [login, setLoginData] = useState<ILogin>({
    email: '',
    password: '',
    agreed: false,
  });
  const {assets, colors, gradients, sizes} = useTheme();

  const handleChange = useCallback(
    (value) => {
      setLoginData((state) => ({...state, ...value}));
    },
    [setLoginData],
  );

  const loginH = async (
    path = 'https://heroo-app.herokuapp.com/auth/login',
    payload = {
      email: login.email,
      password: login.password,
    },
  ) => {
    let result = false;
    return new Promise(function (resolve, reject) {
      axios
        .post(path, payload)
        .then(function (response) {
          // console.log(response.data.first_name);
          if (response?.data?.first_name) {
            setUsr(response?.data?.first_name);
            result = true;
            resolve();
            navigation.navigate('Home');
          } else {
            reject();
          }
          return result;
        })
        .catch(function (error) {
          reject();
        });
    });
  };

  const handleSignIn = useCallback(() => {
    /** send/save registratin data */
    setLoading(true)
    loginH()
      .then(() => {
        setLoading(false)
      })
      .catch((error) => {
        alert('Email si/sau parola gresit(e) !');
        setLoading(false)
      });
  }, [login]);


  useEffect(() => {
    setIsValid((state) => ({
      ...state,
      email: regex.email.test(login.email),
      password: regex.password.test(login.password),
      agreed: login.agreed,
    }));
  }, [login, setIsValid]);

  return (
    <Block safe marginTop={sizes.md}>
      <Block paddingHorizontal={sizes.xs}>
        <Block flex={0} style={{ zIndex: 0 }}>
        </Block>
        {/* login form */}
        <Block
          keyboard
          marginTop={-(sizes.height * 0.02 - sizes.l)}
          behavior={!isAndroid ? 'padding' : 'height'}
        >
          <Block
            flex={0}
            radius={sizes.sm}
            marginHorizontal="8%"
            shadow={!isAndroid} // disabled shadow on Android due to blur overlay + elevation issue
          >
            <Block
              blur
              flex={0}
              intensity={90}
              radius={sizes.sm}
              overflow="hidden"
              justify="space-evenly"
              tint={colors.blurTint}
            //paddingVertical={sizes.sm}
            >
              <Block flex={1} align="center">
                <Image
                  radius={0}
                  width={120}
                  height={1}
                  color={"transparent"}
                  source={assets.logo}
                />
                <Image
                  resizeMode="cover"
                  source={assets.logo}
                  width={120}
                  height={120}
                />
              </Block>
              <Block
                row
                flex={0}
                align="center"
                justify="center"
                marginBottom={sizes.sm}
                paddingHorizontal={sizes.xxl}>
                <Text h4>HEROO</Text>
              </Block>

              <Block
                row
                flex={0}
                align="center"
                justify="center"
                marginBottom={sizes.sm}
                paddingHorizontal={sizes.xxl}>
                <Text center marginHorizontal={sizes.s}>
                  {t('common.callToReg0')} <Text style={{ textDecorationLine: 'underline' }}
                    onPress={() => { navigation.navigate('Register') }}>
                    {t('common.callToReg1')}.
                  </Text>
                </Text>
              </Block>
              {/* form inputs */}
              <Block paddingHorizontal={sizes.sm}>
                <Input
                  autoCapitalize="none"
                  marginBottom={sizes.s}
                  keyboardType="email-address"
                  placeholder={t('common.emailPlaceholder')}
                  success={Boolean(login.email && isValid.email)}
                  danger={Boolean(login.email && !isValid.email)}
                  onChangeText={(value) => handleChange({ email: value })}
                />
                <Input
                  secureTextEntry
                  autoCapitalize="none"
                  marginBottom={sizes.s}
                  placeholder={t('common.passwordPlaceholder')}
                  onChangeText={(value) => handleChange({ password: value })}
                  success={Boolean(login.password && isValid.password)}
                  danger={Boolean(login.password && !isValid.password)}
                />
              </Block>
              <Text center marginHorizontal={sizes.s}>
                {t('common.forgotPass')}
              </Text>
              <Block paddingVertical={sizes.l}>

              </Block>
              <Button
                onPress={handleSignIn}
                marginVertical={sizes.s}
                marginHorizontal={sizes.sm}
                gradient={gradients.black}
              >
                {!loading ? <Text bold white transform="uppercase">
                  {t('common.signin')}
                </Text> : <Text bold white transform="uppercase">
                  Se incearca autentificarea
                </Text>}
              </Button>
            </Block>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Login;
