/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { useTheme } from '../hooks/';
import { Button, Image, Text } from '../components/';
import { StyleSheet, TouchableOpacity, View, ImageBackground } from 'react-native';
import { LogBox } from 'react-native';
import Agentexplorer from './Agentexplorer';
import { useEffect } from 'react';
LogBox.ignoreLogs(['Setting a timer']);
// import { io } from 'socket.io-client';
const Home = () => {
	const [emrgcy, setEmrgcy] = useState<boolean>(false);
	const [agentFound, setAgentFound] = useState<boolean>(false);
	const bgImage = require('../assets/images/bg.jpg')
	//const [emrgcy, setEmrgcy] = useState<boolean>(false)

	const buttonClickedHandler = () => {
		setEmrgcy(true);
		setAgentFound(true)
	};

	const closeClickedHandler = () => {
		setEmrgcy(false);
		setAgentFound(true)
	};

	return (
		<View style={styles.screen}>
			<ImageBackground source={bgImage}
				blurRadius={2}
				resizeMode="cover"
				style={styles.image}>
				<TouchableOpacity
					onPress={buttonClickedHandler}
					style={styles.roundButton2}>
					{!emrgcy ? <Text
						color={"#ffffff"} h1>HEROO</Text> : <Text
							color={"#ffffff"} h3>Searching...</Text>}
				</TouchableOpacity>
				{emrgcy ? <>
					<Agentexplorer cancel={() => setEmrgcy(false)} />
				</> : null}
			</ImageBackground>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	roundButton2: {
		// big button
		width: 200,
		height: 200,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 10,
		borderRadius: 1000,
		backgroundColor: '#007f83',
	},
	roundButton3: {
		top: 0,
		left: 0,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 25,
		margin: 20,
		borderRadius: 1000,
		position: 'absolute',
		backgroundColor: '#ffffff'
	},
	image: {
		flex: 1,
		justifyContent: "center",
		alignItems: 'center',
		width: '100%',
		height: '100%'
	},
});

export default Home;

