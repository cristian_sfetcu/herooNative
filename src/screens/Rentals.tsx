import React, { useCallback, useEffect, useState } from 'react';
import { FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/core';

import { useData, useTheme, useTranslation } from '../hooks/';
import { IArticle } from '../constants/types';
import { Block, Button, Input, Image, Article, Text } from '../components/';

const RentalHeader = () => {
  const { t } = useTranslation();
  const { assets, gradients, sizes } = useTheme();
  return (
    <>
      <Block
        row
        flex={0}
        align="center"
        justify="space-around"
        marginVertical={sizes.m}>
      </Block>

    </>
  );
};

const Rentals = () => {
  const data = useData();
  const { t } = useTranslation();
  const { handleArticle } = data;
  const navigation = useNavigation();
  const { colors, sizes } = useTheme();
  const [notFound, setNotFound] = useState(false);
  const [search, setSearch] = useState('');
  const [recommendations, setRecommendations] = useState<IArticle[]>([]);

  return (
    <Block>
      <Text>{t('screens.rental')}</Text>
    </Block>
  );
};

export default Rentals;
