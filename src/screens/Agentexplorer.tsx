import React, {useState, useEffect} from 'react';
import {Text, Block, Image, Button} from '../components/';
import {useTheme} from '../hooks/';
import {StyleSheet, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Switches from '../components/Switches';
import Sock from '../components/Sock';
function Agentexplorer(props) {
  const {icons} = useTheme();
  const [location, setLocation] = useState<Location.LocationObject | null>(
    null,
  );
  const [emrgcy, setEmrgcy] = useState<boolean>(false);
  const [agentFound, setAgentFound] = useState<boolean>(false);

  // the client's location
  const [locationAgent, setLocationAgent] =
    useState<Location.LocationObject | null>(null); // the agent's location
  const [errorMsg, setErrorMsg] = useState<string | null>(null);

  useEffect(() => {
    console.log('Agentexplorer');
    return () => {
      console.log('Return agentexplorer');
    };
  }, []);
  return (
    <>
      <MapView
        region={{
          //   latitude: location.coords?.latitude,
          //   longitude: location.coords?.longitude,
          latitude: 44.44598,
          longitude: 26.07616,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        style={StyleSheet.absoluteFillObject}
        provider={'google'}>
        {/* <Marker
          coordinate={{
            latitude: location.coords?.latitude,
            longitude: location.coords?.longitude,
          }}
        />
        {agentFound ? (
          <Marker
            coordinate={{
              latitude: locationAgent?.coords.latitude,
              longitude: locationAgent?.coords.longitude,
            }}
          />
        ) : null} */}
      </MapView>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          width: '100%',
        }}>
        <Switches />
      </View>
      <Button primary style={styles.roundButton3} onPress={props.cancel}>
        <Image source={icons.close} radius={0} color={'white'} />
      </Button>
    </>
  );
}
const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  roundButton2: {
    // big button
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 1000,
    backgroundColor: '#007f83',
  },
  roundButton3: {
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 25,
    margin: 20,
    borderRadius: 1000,
    position: 'absolute',
    backgroundColor: '#ffffff',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
});
export default Agentexplorer;
