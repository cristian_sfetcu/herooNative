import * as en from './en.json';
import * as fr from './fr.json';
import * as ro from './ro.json';

export default {
  en,
  fr,
  ro,
};
