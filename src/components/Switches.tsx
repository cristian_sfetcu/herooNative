import React, {useState} from 'react';
import {Text, Block} from '../components/';
import {useTheme} from '../hooks/';

const Switches = () => {
  const {colors, sizes} = useTheme();
  const [agentFound, setAgentFound] = useState<boolean>(false);

  return (
    <Block
      color={colors.card}
      paddingVertical={sizes.m}
      paddingHorizontal={sizes.padding}
      flex={0.1}
      width={'100%'}>
      {!agentFound ? (
        <Text p semibold marginBottom={sizes.s}>
          Searching agent...
        </Text>
      ) : (
        <Block
          row
          flex={0}
          align="center"
          marginTop={12}
          justify="space-between">
          <Text>Agent Name</Text>
          <Text>Ionut Franaru</Text>
        </Block>
      )}
      <Block>{/* {agentFound ?  : null} */}</Block>
    </Block>
  );
};

export default Switches;
